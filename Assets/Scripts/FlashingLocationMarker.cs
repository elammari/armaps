﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.Geospatial;

public class FlashingLocationMarker : MonoBehaviour
{

    public GameObject flashing_Mark;

    public float interval;

    void Start()
    {
        InvokeRepeating("FlashLabel", 0, interval);
    }

    void FlashLabel()
    {
        if (flashing_Mark.activeSelf)
            flashing_Mark.SetActive(false);
        else
            flashing_Mark.SetActive(true);
    }


}
