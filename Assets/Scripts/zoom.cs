﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Microsoft.Maps.Unity;

public class zoom : MonoBehaviour
{
    public Slider zoomSlider;
    public MapRenderer map;
    public Text valueText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SubmitSliderSetting()
    {
        //Displays the value of the slider in the console.
        // Debug.Log(zoomSlider.value);

        map.ZoomLevel = zoomSlider.value;
        valueText.text= zoomSlider.value.ToString();

    }
}
