﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Microsoft.Maps.Unity;
using Microsoft.Geospatial;

public class locateMe : MonoBehaviour
{
    public GameObject flashing_Mark;
    public float interval = 1.0f;
    public GameObject MapObject;
    public MapRenderer map;
 
    public Text gpsNote;

   

    public void Start()
    {

        // turn on location services, if available 
        Input.location.Start();
       InvokeRepeating("FlashLabel", 0, interval);
    }

    public void myCoordinates()
    {
        //Do nothing if location services are not available
        if (Input.location.isEnabledByUser)
        {
            float myLat = Input.location.lastData.latitude;
            float myLon = Input.location.lastData.longitude;
            float myAlt = Input.location.lastData.altitude;
          //  float alt = Input.location.lastData.altitude;
          //  float horzAcc = Input.location.lastData.horizontalAccuracy;
          //  float verticalAcc = Input.location.lastData.verticalAccuracy;
            // float timeStamp = Input.location.lastData.timestamp;

            gpsNote.text = "GPS in on";

         //   latitudeText.text = lat.ToString();
          //  longitudeText.text = lon.ToString();
          //  altitudeText.text = alt.ToString();
          //  horizontalAccuracyText.text = horzAcc.ToString();
          //  verticalAccuracyText.text = verticalAcc.ToString();
          //  timestampText.text = Input.location.lastData.timestamp.ToString();
         
            map.Center = new LatLon(myLat, myLon);
            //  map.ZoomLevel = 19f;
            //map.LocalMapDimension.Set(3,3);
            //  InvokeRepeating("FlashLabel", 0, interval);
            //  Debug.Log("GOT IT ..............");

            //   flashing_Mark.transform.position = new Vector3(MapObject.transform.position.x, MapObject.transform.position.y, 0);


        }
        else
        {
            gpsNote.text = "GPS off";
          //  latitudeText.text = "";
          //  longitudeText.text = "";
          //  altitudeText.text = "";
          //  horizontalAccuracyText.text = "";
          //  verticalAccuracyText.text = "";
           // timestampText.text = "";
        }
        //Debug.Log("DID NOT GET IT ..............");
    }

    void FlashLabel()
    {
        if (flashing_Mark.activeSelf)
            flashing_Mark.SetActive(false);
        else
            flashing_Mark.SetActive(true);
    }
 


}