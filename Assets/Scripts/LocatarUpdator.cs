﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class LocatarUpdator : MonoBehaviour
{


    public Text latitudeText;
    public Text longitudeText;
  //  public Text altitudeText;
  //  public Text horizontalAccuracyText;
  //  public Text verticalAccuracyText;
   // public Text timestampText;


    public Text gpsNote;

    public void Start()
    {

        // turn on location services, if available 
        Input.location.Start();
    }

    public void GetCoordinates()
    {
        //Do nothing if location services are not available
        if (Input.location.isEnabledByUser)
        {
            float lat = Input.location.lastData.latitude;
            float lon = Input.location.lastData.longitude;
    

            gpsNote.text = "GPS in on";

            latitudeText.text = lat.ToString();
            longitudeText.text = lon.ToString();
    
          //  timestampText.text = Input.location.lastData.timestamp.ToString();

            //  Debug.Log("GOT IT ..............");

        }
        else
        {
            gpsNote.text = "GPS off";
            latitudeText.text = "";
            longitudeText.text = "";
           // altitudeText.text = "";
          ////  horizontalAccuracyText.text = "";
          //  verticalAccuracyText.text = "";
          //  timestampText.text = "";
        }
        //Debug.Log("DID NOT GET IT ..............");
    }




}